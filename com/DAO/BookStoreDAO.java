package com.DAO;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.Response;

import entities.Book;
import entities.Card;
import entities.User;

@Stateless
@LocalBean
public class BookStoreDAO {
	
	@PersistenceContext
    private  EntityManager em;
	
	public List<User> getAllUsers() {
		List<User> query = (List<User>)em.createNamedQuery("User.findAll").getResultList();
		return query;
	}
	
	public List<Book> getAllBooks() {
		List<Book> query1 = (List<Book>)em.createNamedQuery("Book.findAll").getResultList();
		return query1;
	}
	
	public Book getBook(int id) {
        return em.find(Book.class, id);
    }
	
	public List<Book> getAllBooksTitle() {
		List<Book> query2 = (List<Book>)em.createNamedQuery("Book.findBytitle").getResultList();
		return query2;
	}
	
	public List<Book> getAllBooksAuthor() {
		List<Book> query3 = (List<Book>)em.createNamedQuery("Book.findByauthor").getResultList();
		return query3;
	}
	
	public List<Book> getAllBooksCategory() {
		List<Book> query4 = (List<Book>)em.createNamedQuery("Book.findBycategory").getResultList();
		return query4;
	}
	
	public List<Book> getAllBooksPrice() {
		List<Book> query4 = (List<Book>)em.createNamedQuery("Book.findByprice").getResultList();
		return query4;
	}
	
	
	public Response register(User user) throws URISyntaxException {
		em.persist(user);
		URI uri = new URI("http://localhost:8080/BookStore/card.jsp");
		return Response.temporaryRedirect(uri).build();
	}
	
	public Response addCard(Card card) throws URISyntaxException {
		em.persist(card);
		URI uri = new URI("http://localhost:8080/BookStore/login.jsp");
		return Response.temporaryRedirect(uri).build();
	}

}
