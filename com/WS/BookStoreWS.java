package com.WS;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.Form;

import com.DAO.BookStoreDAO;

import entities.AbstractUser;
import entities.Book;
import entities.Card;
import entities.NullUser;
import entities.User;

@Path("/BookStore")
@Stateless
@LocalBean
public class BookStoreWS {
	
	User currentUser = new User();
	Book selectedBook = new Book();

	@EJB
	private BookStoreDAO bookDao;

	@Path("addUser")
	@POST
	public Object addUser(@Form User form) throws URISyntaxException {

		User user = new User();
		user.setName(form.getName());
		user.setUsername(form.getUsername());
		user.setPassword(form.getPassword());
		user.setEnabled(true);
		user.setAuthority("USER");
		user.setShippingAddress(form.getShippingAddress());

		return bookDao.register(user);
	}
	
	ArrayList<Book> books = new ArrayList<Book>();
	
	@Path("addToShoppingCart")
	@GET
	public Response addBook(@Form Book form) throws URISyntaxException{
		for (Book b: books){
			if (b.getTitle().equalsIgnoreCase(form.getTitle()))
					{
						selectedBook.setTitle(form.getTitle());
						URI uri = new URI("http://loclahost:8080/BookStore/ShoppingCart.jsp");
						return Response.temporaryRedirect(uri).build();
					}
		}
		return null;
	}

	ArrayList<User> users = new ArrayList<User>();

	@Path("login")
	@POST
	public Response login(@Form User form) throws URISyntaxException {
		bookDao.getAllUsers();
		for (User td : bookDao.getAllUsers()) {
			System.out.println("This is a user " + td.toString());
			users.add(td);
		}
		for (User u : users) {
			System.out.println("this is u:" + u);
			if (u.getUsername().equalsIgnoreCase(form.getUsername())
					&& u.getPassword().equalsIgnoreCase(form.getPassword())) {
				currentUser.setUsername(form.getUsername());
				currentUser.setPassword(form.getPassword());
				URI uri = new URI(
						"http://localhost:8080/BookStore/HomePage.jsp");
				return Response.temporaryRedirect(uri).build();
			}
		}
		return null;
	}
	
	@Path("card")
	@POST
	public Object addCard(@Form Card form) throws URISyntaxException
	{
		Card c = new Card();
		c.setCardNumber(form.getCardNumber());
		c.setSecurityNumber(form.getSecurityNumber());
		c.setExpiryMonth(form.getExpiryMonth());
		c.setExpiryYear(form.getExpiryYear());
		c.setCardHolder(form.getCardHolder());
		
		return bookDao.addCard(c);
	}
	
	@GET
	@Path("/json/books")
	@Produces("application/json")
	public List<Book> getAllBooksJson() {
		return bookDao.getAllBooks();
	}
	
	@GET
	@Path("/json/bookstitles")
	@Produces("application/json")
	public List<Book> getAllBookstitleJson() {
		return bookDao.getAllBooksTitle();
	}
	
	@GET
	@Path("/json/books/authors")
	@Produces("application/json")
	public List<Book> getAllBooksAuthorJson() {
		return bookDao.getAllBooksAuthor();
	}
	
	@GET
	@Path("/json/books/categorys")
	@Produces("application/json")
	public List<Book> getAllBooksCategoryJson() {
		return bookDao.getAllBooksCategory();
	}
	
	@GET
	@Path("/json/books/price")
	@Produces("application/json")
	public List<Book> getAllBooksPriceJson() {
		return bookDao.getAllBooksPrice();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public Book getBook(@PathParam("id") int id) {
	    return bookDao.getBook(id);
	}
	

}
