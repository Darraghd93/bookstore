package entities;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class PurchaseHistory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String date;
	
	@OneToOne(cascade = {CascadeType.ALL})
	private ShoppingCart shooppingcart;
	
	public PurchaseHistory() {
		
	}

	public PurchaseHistory(String date, ShoppingCart shooppingcart) {
		super();
		this.date = date;
		this.shooppingcart = shooppingcart;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public ShoppingCart getShooppingcart() {
		return shooppingcart;
	}

	public void setShooppingcart(ShoppingCart shooppingcart) {
		this.shooppingcart = shooppingcart;
	}
	
	
	
	
	
	
	

}
