package entities;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.ws.rs.FormParam;



@Entity
public class Card {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@FormParam("cardNumber")
	private String cardNumber;
	@FormParam("securityNumber")
	private String securityNumber;
	@FormParam("expiryMonth")
	private String expiryMonth;
	@FormParam("expiryYear")
	private String expiryYear;
	@FormParam("cardHolder")
	private String cardHolder;
	
	
	public Card() {
	
	}


	public Card(String cardNumber, String securityNumber, String expiryMonth,
			String expiryYear, String cardHolder) {
		super();
		this.cardNumber = cardNumber;
		this.securityNumber = securityNumber;
		this.expiryMonth = expiryMonth;
		this.expiryYear = expiryYear;
		this.cardHolder = cardHolder;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getCardNumber() {
		return cardNumber;
	}


	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}


	public String getSecurityNumber() {
		return securityNumber;
	}


	public void setSecurityNumber(String securityNumber) {
		this.securityNumber = securityNumber;
	}


	public String getExpiryMonth() {
		return expiryMonth;
	}


	public void setExpiryMonth(String expiryMonth) {
		this.expiryMonth = expiryMonth;
	}


	public String getExpiryYear() {
		return expiryYear;
	}


	public void setExpiryYear(String expiryYear) {
		this.expiryYear = expiryYear;
	}


	public String getCardHolder() {
		return cardHolder;
	}


	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}
	
	
	

}
