package entities;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.ws.rs.FormParam;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@NamedQueries( {
	@NamedQuery(name = "User.findAll", query = "select o from User o"),
	//@NamedQuery(name = "User.findByname", query = "select o from User o where o.name=:name"),
	//@NamedQuery(name = "User.findByemail", query = "select o from User o where o.email=:email"),
	//@NamedQuery(name = "User.findByusername", query = "select o from User o where o.username=:username"),
	})

@Entity
@XmlRootElement
public class User extends AbstractUser {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@FormParam("name")
	private String name;
	@FormParam("username")
	private String username;
	@FormParam("password")
	private String password;
	private Boolean enabled = false;
	private String authority;
	@FormParam("shippingAddress")
	private String shippingAddress;
	
	@OneToMany(cascade = {CascadeType.ALL},fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	List<PurchaseHistory> purchase = new ArrayList<PurchaseHistory>();
	
	@OneToOne
	private Card card;
	
	@OneToOne
	private ShoppingCart shoppingCart;
	
	public User()
	{
		
	}

	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}





	public User(String name, String username, String password,
			Boolean enabled, String authority, String shippingAddress) {
		super();
		this.name = name;
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.authority = authority;
		this.shippingAddress = shippingAddress;
	}

	public User(String name, String username, String password, Boolean enabled,
			String authority, String shippingAddress,
			List<PurchaseHistory> purchase, Card card, ShoppingCart shoppingCart) {
		super();
		this.name = name;
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.authority = authority;
		this.shippingAddress = shippingAddress;
		this.purchase = purchase;
		this.card = card;
		this.shoppingCart = shoppingCart;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public List<PurchaseHistory> getPurchase() {
		return purchase;
	}

	public void setPurchase(List<PurchaseHistory> purchase) {
		this.purchase = purchase;
	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public ShoppingCart getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(ShoppingCart shoppingCart) {
		this.shoppingCart = shoppingCart;
	}

	@Override
	public boolean isNil() {
		// TODO Auto-generated method stub
		return false;
	}

}
