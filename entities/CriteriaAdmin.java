package entities;

import java.util.ArrayList;
import java.util.List;

public class CriteriaAdmin implements Criteria {

   @Override
   public List<User> meetCriteria(List<User> users) {
      List<User> adminUsers = new ArrayList<User>(); 
      
      for (User u : users) {
         if(u.getAuthority().equalsIgnoreCase("ADMIN")){
            adminUsers.add(u);
         }
      }
      return adminUsers;
   }


}
