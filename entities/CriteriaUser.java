package entities;

import java.util.ArrayList;
import java.util.List;

public class CriteriaUser implements Criteria {

   @Override
   public List<User> meetCriteria(List<User> users) {
      List<User> normalUsers = new ArrayList<User>(); 
      
      for (User u : users) {
         if(u.getAuthority().equalsIgnoreCase("USER")){
            normalUsers.add(u);
         }
      }
      return normalUsers;
   }
}