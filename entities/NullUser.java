package entities;

public class NullUser extends AbstractUser {

	   @Override
	   public String getName() {
	      return "Not Available in Customer Database";
	   }

	   @Override
	   public boolean isNil() {
	      return true;
	   }
	}