package entities;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@NamedQueries( {
	@NamedQuery(name = "Book.findAll", query = "select o from Book o"),
	@NamedQuery(name = "Book.findBytitle", query = "select title from Book o"),
	@NamedQuery(name = "Book.findByauthor", query = "select author from Book o"),
	@NamedQuery(name = "Book.findBycategory", query = "select category from Book o"),
	@NamedQuery(name = "Book.findByprice", query = "select price from Book o"),
	})


@Entity
public class Book {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String title;
	private String author;
	private double price;
	private String category;
	private String image;
	private int quantity;
	private String totalRating;
	
	@OneToMany(cascade = {CascadeType.ALL},fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	List<Review> reviews = new ArrayList<Review>();
	
	
	
	@OneToOne(cascade = {CascadeType.ALL})
	private Stock stock;
	
	public Book() {
		
	}

	

	public Book(String title, String author, double price, String category,
			String image, int quantity, String totalRating,
			List<Review> reviews,  Stock stock) {
		super();
		this.title = title;
		this.author = author;
		this.price = price;
		this.category = category;
		this.image = image;
		this.quantity = quantity;
		this.totalRating = totalRating;
		this.reviews = reviews;
		
		this.stock = stock;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getTotalRating() {
		return totalRating;
	}

	public void setTotalRating(String totalRating) {
		this.totalRating = totalRating;
	}

	public List<Review> getReviews() {
		return reviews;
	}

	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

}
